FROM python:2.7.14

ADD src/ /
RUN pip install -r /requirements.txt
CMD ["python", "/server.py"]
